package com.example.demo.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class DemoController {
	@GetMapping(value = "/")
	public String getCities() {

		return "Demo Application is running fine";
	}
}
